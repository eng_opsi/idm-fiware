package it.eng.digitalenabler.idm.fiware;

import it.eng.digitalenabler.identity.manager.interfaces.OAuth2Provider;
import it.eng.digitalenabler.identity.manager.interfaces.RoleManager;
import it.eng.digitalenabler.identity.manager.model.Credentials;
import it.eng.digitalenabler.identity.manager.model.Token;
import it.eng.digitalenabler.restclient.RestClient;
import it.eng.digitalenabler.sdk.databinder.model.DataBinder;


public abstract class IdentityManager implements OAuth2Provider,RoleManager {
		protected static String baseUrl;
		
		protected static Credentials adminCredentials;
		protected Token adminToken;
		
		protected static RestClient restClient = null;
		protected static DataBinder jsonSerde = null;
		
		public abstract Token getAdminToken();
		
		public static Credentials getAdminCredentials() {
			return adminCredentials;
		}

		public static void setAdminCredentials(String username,String password) {
			IdentityManager.adminCredentials = new Credentials(username,password);
		}
		
		public static String getBaseUrl() {
			return IdentityManager.baseUrl;
		}
		public static void setBaseUrl(String url) {
			IdentityManager.baseUrl = url;
		}
		
		public static void initialize(RestClient rClient,DataBinder jSerde) {
			setRestClient(rClient);
			setJsonSerde(jSerde);
		}	

		public static RestClient getRestClient() {
			return restClient;
		}

		public static void setRestClient(RestClient restClient) {
			IdentityManager.restClient = restClient;
		}

		public static DataBinder getJsonSerde() {
			return jsonSerde;
		}

		public static void setJsonSerde(DataBinder jsonSerde) {
			IdentityManager.jsonSerde = jsonSerde;
		}
}
